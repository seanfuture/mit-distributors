<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpmitdistributors');

/** MySQL database username */
define('DB_USER', 'mitdistributors');

/** MySQL database password */
define('DB_PASSWORD', 'RqQjJZYrWfgHja3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+YT=ser4/3-sPc0Z.,?+B=X$>:H%6<wi7P`o^#c#CM>~_kDX^Y!0$6Sp8B$@@$J|');
define('SECURE_AUTH_KEY',  't97o}8r^:05+sIzOCCV|PRp-Ag-+uGH5TVN}$$)<jUKwy3yugyrvB @+]=Wu!%77');
define('LOGGED_IN_KEY',    ',TkJ*3tNL0}B-gE_F(XT)@Uq[]rU8cSrwJAYY)v|KEi3}TnFz62=Cg8HTYhWk;@ ');
define('NONCE_KEY',        'KwM|WI2a+Wch(b>vkM9SQ>_h;|ds(=AcyI.+/R{}HY_EZ,33;Ru|G%+v ]J5rro6');
define('AUTH_SALT',        'i|+NK3Y@o%:v~{o]+d>w*Idi`JlI^q<g}~d}P<O59+--I/|IbZ1#n-[kF7|:UIL=');
define('SECURE_AUTH_SALT', 'jEBNj9uo.`{u#mffQ!LxmG]Z/?I1OlZLyE22nEY1VA6F&EbQbI?4%,+I/g(HD5FS');
define('LOGGED_IN_SALT',   'CBwqZrCfApSRu3[H+U;xqaBei+h+pIY0@/Q!rpK|L!;0kC.>6L*=C+9mENvyWnq+');
define('NONCE_SALT',       '<I4<lqw*}O:1tj>H(S<[2vA*~|=`_/Tj|-VkiY(`xGT3v;W0(/d+]T`*@h0tYb{K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
