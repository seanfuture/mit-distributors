<?php
/**
 * Template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

	<div class="header-img-container">
		<div class="header-top-line"></div>
		<div class="header-img"></div>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); endwhile; ?>

	<?php get_footer(); ?>