<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */
?>

		<section id="contact" class="section-padding">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-offset-2">
						 <div class="text-center">
					<h2 class="center-title">Contact Us</h2>
					<span class="center-border-line"></span>
					<p>&nbsp;</p>
					<!-- <p class="sub-text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in piece of classical Latin literature from 45 BC, it a old.</p> -->
				</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7">
						<div class="contact-form wow animated fadeInLeft" data-wow-delay=".2s">

							<form role="form" id="feedbackForm">
								<div class="row">
									<div class="col-md-9">
										<div class="form-group">
											<label class="control-label" for="name">Name *</label>
											<div>
												<input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" />

											</div>
											<span class="help-block" style="display: none;">Please enter your name.</span>
										</div>
									</div>
								</div>
								 <div class="row">
								 <div class="col-md-9">
									<div class="form-group">
									<label class="control-label" for="email">Email Address *</label>
									<div>
										<input type="email" class="form-control" id="email" name="email" placeholder="Enter your email" />

									</div>
									<span class="help-block" style="display: none;">Please enter a valid e-mail address.</span>
								</div>
								 </div>
								 </div>
								<div class="form-group">
									<label class="control-label" for="message">Message *</label>
									<div>
										<textarea rows="5" cols="30" class="form-control" id="message" name="message" placeholder="Enter your message" ></textarea>

									</div>
									<span class="help-block" style="display: none;">Please enter a message.</span>
								</div>
								<img id="captcha" src="/assets/form/library/vender/securimage/securimage_show.php" alt="CAPTCHA Image" />
								<a href="#" onclick="document.getElementById('captcha').src = 'assets/form/library/vender/securimage/securimage_show.php?' + Math.random();
										return false" class="btn btn-info btn-sm"><i class="ion-refresh"></i> Show a Different Image</a><br/>
								<div class="form-group" style="margin-top: 10px;">
									<label class="control-label" for="captcha_code">Text Within Image *</label>
									<div>
										<input type="text" class="form-control" name="captcha_code" id="captcha_code" placeholder="For security, please enter the code displayed in the box." />

									</div>
									<span class="help-block" style="display: none;">Please enter the code displayed within the image.</span>
								</div>
								<span class="help-block" style="display: none;">Please enter a the security code.</span>
								<button type="submit" id="feedbackSubmit" class="btn btn-dark-bg btn-lg" data-loading-text="Sending..." style="display: block; margin-top: 10px;">
									Contact Us
								</button>
							</form>
						</div><!--contact form-->
					</div><!--col-7-->
					<div class="col-sm-5">
						<h4>Love to hear from you!</h4>
						<p>
							We’d like to learn more about your needs and see if our products or services are a fit. Contact us below or call 1-800-MIT-FIND.
						</p>
						<br>
						<br>
						<!-- <h4>Our Locations</h4> -->

<!--             <p>
							MIT Distributors<br>
							205 Hembree Park Drive Suite 105<br>
							Roswell, GA 30076<br>
							1-800-MIT-FIND<br>
							<a href="mailto:support@mitfind.com">support@mitfind.com</a>
						</p> -->

						<div class="row">
						<div class="col-sm-10 col-sm-offset-1">

						<div class="contact-cta">
							<i class="ion-android-call"></i>
							<h4>1-800-MIT-FIND</h4>
						</div>
						<div class="contact-cta">
							<i class="ion-email"></i>
							<h4><a href="mailto:support@mitfind.com">support@mitfind.com</a></h4>
						</div>
						<div class="contact-cta">
							<i class="ion-bookmark"></i>
							<h4><a href="http://www.mitfind.com">http://mitfind.com</a></h4>
						</div>

						<!-- <p>
							MIT Distributors<br>
							205 Hembree Park Dr Suite 105<br>
							Roswell, GA 30076<br>
							800-MIT-FIND<br>
						</p>
						<p>
							MIT Distributors<br>
							12300 Ford Rd Suite B306<br>
							Farmers Branch, Texas 75234<br>
							800-MIT-FIND<br>
						</p>
						</div>
						<div class="col-sm-6">
						<p>
							MIT Distributors<br>
							701 Ford Rd Suite 6<br>
							Rockaway, NJ 07866<br>
							800-MIT-FIND<br>
						</p>
						<p>
							MIT Distributors<br>
							4611 S. 96th Street Suite 158<br>
							Omaha, NE 68127<br>
							800-MIT-FIND<br>
						</p> -->

						</div>
						</div>

					</div>
				</div><!--row-->
			</div><!--container-->
		</section><!--contact section end-->

		<!-- <div class="numbers-cta">
			<div class="container">
				<div class="col-sm-12 col-md-4 margin-bottom30 text-center">
					<i class="ion-android-call"></i>
					<h3>1-800-MIT-FIND</h3>
				</div>
				<div class="col-sm-12 col-md-4 margin-bottom30 text-center">
					<i class="ion-email"></i>
					<h3><a href="mailto:support@mitfind.com">support@mitfind.com</a></h3>
				</div>
				<div class="col-sm-12 col-md-4 margin-bottom30 text-center">
					<i class="ion-bookmark"></i>
					<h3><a href="http://www.mitfind.com">http://mitfind.com</a></h3>
				</div>
			</div>
		</div> -->

		<div class="numbers-cta">

			<h2 class="center-title">Our Locations</h2>
			<span class="center-border-line"></span>

			<div class="container locations-container">
				<div class="col-sm-6 col-md-3 margin-bottom30 text-center">
					<i class="ion-location"></i>
					<h3>
						MIT Distributors<br>
						205 Hembree Park Dr Suite 105<br>
						Roswell, GA 30076<br>
						<!-- 800-MIT-FIND -->
					</h3>
				</div>
				<div class="col-sm-6 col-md-3 margin-bottom30 text-center">
					<i class="ion-location"></i>
					<h3>
						MIT Distributors<br>
						12300 Ford Rd Suite B306<br>
						Farmers Branch, Texas 75234<br>
						<!-- 800-MIT-FIND -->
					</h3>
				</div>
				<div class="col-sm-6 col-md-3 margin-bottom30 text-center">
					<i class="ion-location"></i>
					<h3>
						MIT Distributors<br>
						701 Ford Rd Suite 6<br>
						Rockaway, NJ 07866<br>
						<!-- 800-MIT-FIND -->
					</h3>
				</div>
				<div class="col-sm-6 col-md-3 margin-bottom30 text-center">
					<i class="ion-location"></i>
					<h3>
						MIT Distributors<br>
						4611 S. 96th Street Suite 158<br>
						Omaha, NE 68127<br>
						<!-- 800-MIT-FIND -->
					</h3>
				</div>
			</div>
		</div>

		<footer class="footer">
			<?php get_sidebar( 'footer' ); ?>
			<div class="container text-center">
				<!-- <a href="#"><img src="/assets/images/mit/logo.png" alt=""></a> -->
				<!-- <ul class="list-inline socials">
					<li><a href="#" class="fb"><i class="ion-social-facebook-outline"></i></a></li>
					<li><a href="#" class="twit"><i class="ion-social-twitter-outline"></i></a></li>
					<li><a href="#" class="g-plus"><i class="ion-social-googleplus-outline"></i></a></li>
					<li><a href="#" class="yout"><i class="ion-social-youtube-outline"></i></a></li>
					<li><a href="#" class="drib"><i class="ion-social-dribbble-outline"></i></a></li>
				</ul> -->
				<p>&copy; 2015. All rights reserved.</p>
			</div>
		</footer><!--footer end-->

		<!--jquery-->
		<script src="/assets/js/jquery.min.js" type="text/javascript"></script>
		<script src="/assets/js/jquery-migrate.min.js" type="text/javascript"></script>
		<script src="/assets/js/moderniz.min.js" type="text/javascript"></script>
		<script src="/assets/js/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		 <script type="text/javascript" src="/assets/js/bootstrap-hover-dropdown.min.js"></script>
		<script type="text/javascript" src="/assets/js/jquery.flexslider-min.js"></script>
		<script type="text/javascript" src="/assets/js/parallax.min.js"></script>
		<script src="/assets/js/tweetie.min.js" type="text/javascript"></script>
		<script src="/assets/js/waypoints.min.js"></script>
		<script src="/assets/js/jquery.sticky.js" type="text/javascript"></script>
		<script src="/assets/js/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="/assets/js/wow.min.js" type="text/javascript"></script>
		<script src="/assets/js/template.js" type="text/javascript"></script>
		<script src="/assets/js/contact-form.js" type="text/javascript"></script>
		<script src="/assets/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
		<script src="/assets/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
		<script src="/assets/js/revolution-custom.js" type="text/javascript"></script>
		<script src="/assets/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
		<script src="/assets/js/cube-portfolio.js" type="text/javascript"></script>
		<script src="/assets/js/pricing.js" type="text/javascript"></script>
		<!--master slider-->
		<script src="/assets/master-slider/js/masterslider.min.js" type="text/javascript"></script>
		<script src="/assets/js/masterslider-custom.js" type="text/javascript"></script>

		<?php
		/*
		 * Always have wp_footer() just before the closing </body>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to reference JavaScript files.
		 */
		wp_footer();
	?>
	</body>
</html>