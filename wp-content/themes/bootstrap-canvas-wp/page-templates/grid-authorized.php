<?php
/**
 * Template Name: Authorized Lines Template
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

	<div class="header-img-container">
		<div class="header-top-line"></div>
		<div class="header-img"></div>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); endwhile; ?>

		<section id="work" class="section-padding wow animated fadeIn" data-wow-delay=".1s">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="text-center">
							<h2 class="center-title">Authorized Lines</h2>
							<span class="center-border-line"></span>
							<p class="sub-text">
							MIT Distributors has franchise agreements with some of the industry's best known manufacturing companies, brands such as Microtips Technology, Zeus Battery, Nikkohm, Central Technologies, Diptronics, and many more.
							</p>
						</div>
					</div>
				</div><!--center title row end-->
			</div><!--container-->

			<div class="container products-container">

				<div id="filters-container" class="cbp-l-filters-dropdown">
					<div class="cbp-l-filters-dropdownWrap">
						<div class="cbp-l-filters-dropdownHeader">Sort Products</div>
						<div class="cbp-l-filters-dropdownList">
							<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
								All ( <div class="cbp-filter-counter"></div> product lines )
							</div>
							<!-- <div data-filter=".print" class="cbp-filter-item">
								Print (<div class="cbp-filter-counter"></div> items)
							</div>
							<div data-filter=".web-design" class="cbp-filter-item">
								Web Design (<div class="cbp-filter-counter"></div> items)
							</div>
							<div data-filter=".motion" class="cbp-filter-item">
								Motion (<div class="cbp-filter-counter"></div> items)
							</div> -->
						</div>
					</div>
				</div>

				<div id="grid-container" class="cbp">

<!-- 					<div class="cbp-item print motion">
						<a href="/assets/products/001.html" class="cbp-caption cbp-singlePageInline"
						   data-title="3M Static Control" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image001.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											3M Static Control
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div> -->

					<div class="cbp-item print motion">
						<a href="/assets/products/002.html" class="cbp-caption cbp-singlePageInline"
						   data-title="VIA Technologies" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image002.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											VIA Technologies
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/003.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Viking America" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image003.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Viking America
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/004.html" class="cbp-caption cbp-singlePageInline"
						   data-title="TGS" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image004.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											TGS
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/005.html" class="cbp-caption cbp-singlePageInline"
						   data-title="EEMB" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image005.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											EEMB
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/006.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Orient Display (USA) Corporation" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image006.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Orient Display (USA) Corporation
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/007.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Nikkohm" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image007.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Nikkohm
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/008.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Central Technologies" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image008.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Central Technologies
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/009.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Barker MicroFarad, Inc." onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image009.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Barker MicroFarad, Inc.
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/010.html" class="cbp-caption cbp-singlePageInline"
						   data-title="MicroTips Technology" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image010.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											MicroTips Technology
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/011.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Major League Electronics" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image011.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Major League Electronics
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/012.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Advantech" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image012.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Advantech
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/013.html" class="cbp-caption cbp-singlePageInline"
						   data-title="DDP" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/add001.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											DDP
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/014.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Evergreen" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical inset">
								<img src="/assets/images/products/add002.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Evergreen
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/015.html" class="cbp-caption cbp-singlePageInline"
						   data-title="GigaDevice" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/add003.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											GigaDevice
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/016.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Jauch" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/add004.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Jauch
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/017.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Mornsun" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical shifted-down-further">
								<img src="/assets/images/products/add005.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Mornsun
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/018.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Okaya" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical inset">
								<img src="/assets/images/products/add006.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Okaya
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/019.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Quectel" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical inset">
								<img src="/assets/images/products/add007.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Quectel
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/020.html" class="cbp-caption cbp-singlePageInline"
						   data-title="MEGA Electronics" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image020.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											MEGA Electronics
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/021.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Raltron Electronics, Inc." onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image021.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Raltron Electronics, Inc.
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/022.html" class="cbp-caption cbp-singlePageInline"
						   data-title="ZEUS Battery Products" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image022.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											ZEUS Battery Products
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/023.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Diptronics Manufacturing" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image023.jpg" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Diptronics Manufacturing
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/024.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Wieland Electronic Products" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical-partial inset">
								<img src="/assets/images/products/image024.jpg" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Wieland Electronic Products
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/025.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Samxon" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical-partial inset shifted-down">
								<img src="/assets/images/products/add008.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Samxon
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/026.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Paralight Corp" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image026.jpg" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Paralight Corp
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/027.html" class="cbp-caption cbp-singlePageInline"
						   data-title="American Opto Plus LED Corp" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image027.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											American Opto Plus LED Corp
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/028.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Arbor Technologies" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<img src="/assets/images/products/image028.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Arbor Technologies
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/029.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Vacuumschmelze" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap reduced-size">
								<img src="/assets/images/products/add009.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Vacuumschmelze
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/030.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Tekmos Electronics" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical-partial inset">
								<img src="/assets/images/products/image030.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Tekmos Electronics
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/031.html" class="cbp-caption cbp-singlePageInline"
						   data-title="CoolTron" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical-partial inset">
								<img src="/assets/images/products/image031.jpg" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											CoolTron
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/products/032.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Wall Industries" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap image-vertical-partial inset">
								<img src="/assets/images/products/add010.png" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Wall Industries
										</div>
										<div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

				</div>

				<div id="loadMore-container" class="cbp-l-loadMore-button">
					<a href="#rapidquote" class="fancybox cbp-l-loadMore-link">
						<span class="cbp-l-loadMore-defaultText">RAPID QUOTE</span>
						<span class="cbp-l-loadMore-loadingText">LOADING...</span>
						<span class="cbp-l-loadMore-noMoreLoading">RAPID QUOTE</span>
					</a>
				</div>

			</div><!--container-->
		</section><!--portfolio section end here-->


	<?php get_footer(); ?>