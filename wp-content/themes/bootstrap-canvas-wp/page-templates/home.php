<?php
/**
 * Template Name: Home Page Template
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

get_header(); ?>

<div class="container">
<div class="fullwidthbanner" id="home">
	<div class="tp-banner">
		<ul>

			<!-- SLIDE  -->
			<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="assets/images/mit/home2.png"
				data-delay="10000"  data-saveperformance="off" data-title="Authorized Lines">

				<!-- MAIN IMAGE -->
				<img src="assets/images/mit/home2.png"  alt=""  data-bgposition="center center" data-kenburns="on"
					data-duration="10000" data-ease="Linear.easeNone" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center center">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption srcaption-bigwhite lfl ltr slider-title reduced"
					 data-x="right" data-hoffset="90"
					 data-y="top" data-voffset="80"
					 data-speed="1000"
					 data-start="100"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 2">authorized <strong>lines</strong>
				</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption srcaption-miniwhite sfb slider-caption"
					 data-x="right" data-hoffset="90"
					 data-y="top" data-voffset="125"
					 data-speed="500"
					 data-start="1200"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3">See our portfolio
				</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption srcaption-miniwhite sfb text-light scroll-to slider-arrow-down"
					 data-x="right" data-hoffset="20"
					 data-y="top" data-voffset="160"
					 data-speed="500"
					 data-start="1300"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3"><a href="/authorized-lines" class="sr-button sr-buttonicon small-iconbutton scroll-to"><i class="ion-ios-arrow-down"></i></a>
				</div>

			</li>

			<!-- SLIDE  -->
			<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="assets/images/mit/globalhawk.jpg"
				data-delay="10000"  data-saveperformance="off" data-title="MIT Distributors">

				<!-- MAIN IMAGE -->
				<img src="assets/images/mit/globalhawk.jpg"  alt=""  data-bgposition="left center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption srcaption-bigwhite lfl ltr slider-title"
					 data-x="center" data-hoffset="0"
					 data-y="center" data-voffset="0"
					 data-speed="1000"
					 data-start="100"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 2"><strong>MIT</strong> Distributors
				</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption srcaption-miniwhite sfb slider-caption"
					 data-x="center" data-hoffset="0"
					 data-y="bottom" data-voffset="-140"
					 data-speed="500"
					 data-start="1200"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3">Learn more about us
				</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption srcaption-miniwhite sfb text-light scroll-to slider-arrow-down"
					 data-x="center" data-hoffset="0"
					 data-y="bottom" data-voffset="-60"
					 data-speed="500"
					 data-start="1300"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3"><a href="/about" class="sr-button sr-buttonicon small-iconbutton scroll-to"><i class="ion-ios-arrow-down"></i></a>
				</div>

			</li>

			<!-- SLIDE  -->
			<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="assets/images/banners/banner6.jpg"
				data-delay="10000"  data-saveperformance="off" data-title="Product Lines">

				<!-- MAIN IMAGE -->
				<img src="assets/images/banners/banner6.jpg"  alt=""  data-bgposition="left top" data-kenburns="on" data-duration="10000"
					data-ease="Linear.easeNone" data-bgfit="115" data-bgfitend="100" data-bgpositionend="left top">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption srcaption-bigwhite lfl ltr slider-title"
					 data-x="left" data-hoffset="-90"
					 data-y="top" data-voffset="110"
					 data-speed="1000"
					 data-start="100"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 2">industry<sup class="hyp">'</sup>s <strong>best</strong>
				</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption srcaption-miniwhite sfb slider-caption"
					 data-x="left" data-hoffset="-85"
					 data-y="top" data-voffset="170"
					 data-speed="500"
					 data-start="1200"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3">Product lines
				</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption srcaption-miniwhite sfb text-light scroll-to slider-arrow-down"
					 data-x="left" data-hoffset="-85"
					 data-y="top" data-voffset="210"
					 data-speed="500"
					 data-start="1300"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3"><a href="/products" class="sr-button sr-buttonicon small-iconbutton scroll-to"><i class="ion-ios-arrow-down"></i></a>
				</div>

			</li>

			<!-- SLIDE  -->
			<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="assets/images/mit/printed-circuit-board.jpg"
				data-delay="10000"  data-saveperformance="off" data-title="Scalable Services">

				<!-- MAIN IMAGE -->
				<img src="assets/images/mit/printed-circuit-board.jpg"  alt=""  data-bgposition="right center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption srcaption-bigwhite lfl ltr slider-title"
					 data-x="center" data-hoffset="0"
					 data-y="center" data-voffset="0"
					 data-speed="1000"
					 data-start="100"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 2">scalable <strong>services</strong>
				</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption srcaption-miniwhite sfb slider-caption"
					 data-x="center" data-hoffset="0"
					 data-y="bottom" data-voffset="-140"
					 data-speed="500"
					 data-start="1200"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3">Find out more
				</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption srcaption-miniwhite sfb text-light scroll-to slider-arrow-down"
					 data-x="center" data-hoffset="0"
					 data-y="bottom" data-voffset="-60"
					 data-speed="500"
					 data-start="1300"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3"><a href="/services" class="sr-button sr-buttonicon small-iconbutton scroll-to"><i class="ion-ios-arrow-down"></i></a>
				</div>

			</li>

			<!-- SLIDE  -->
			<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="assets/images/mit/F-22.jpg"
				data-delay="10000"  data-saveperformance="off" data-title="Dedicated Support">

				<!-- MAIN IMAGE -->
				<img src="assets/images/mit/F-22.jpg"  alt=""  data-bgposition="left center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">

				<!-- LAYER NR. 1 -->
				<div class="tp-caption srcaption-bigwhite lfl ltr slider-title"
					 data-x="center" data-hoffset="0"
					 data-y="center" data-voffset="0"
					 data-speed="1000"
					 data-start="100"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 2">dedicated <strong>support</strong>
				</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption srcaption-miniwhite sfb slider-caption"
					 data-x="center" data-hoffset="0"
					 data-y="bottom" data-voffset="-140"
					 data-speed="500"
					 data-start="1200"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3">Get in touch with us
				</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption srcaption-miniwhite sfb text-light scroll-to slider-arrow-down"
					 data-x="center" data-hoffset="0"
					 data-y="bottom" data-voffset="-60"
					 data-speed="500"
					 data-start="1300"
					 data-easing="easeInOutQuad"
					 data-endspeed="1000"
					 data-endeasing="easeInOutQuad"
					 style="z-index: 3"><a href="#contact" class="sr-button sr-buttonicon small-iconbutton scroll-to"><i class="ion-ios-arrow-down"></i></a>
				</div>

			</li>

		</ul>
	</div>
</div><!--full width banner-->
</div>

<!--sub title center-->
<div class="certifications">
<div class="container">
<div class="row">
	<div class="col-sm-4">
		<div class="service-features wow animated fadeIn" data-wow-delay=".2s">
			<div class="img-container">
				<a target="_blank" href="/assets/documents/MIT-Distributors-AS9120-2015.pdf"><img src="/assets/images/mit/ISO9000.png"></a>
			</div>
			<!-- <i class="ion-ios-lightbulb-outline"></i> -->
			<!-- <h3>ISO 9000 Certified</h3> -->
			<p>
				Certification
approves MIT Distributors’ quality management standards established by the International
Organization for Standardization.
			</p>
		</div><!--service features-->
		<div class="vertical-border-line-container">
			<div class="vertical-border-line"></div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="service-features wow animated fadeIn" data-wow-delay=".3s">
			<div class="img-container">
				<a target="_blank" href="/assets/documents/MIT-Distributors-AS9120-2015.pdf"><img src="/assets/images/mit/AS9120.png"></a>
			</div>
			<!-- <i class="ion-ios-people-outline"></i> -->
			<!-- <h3>AS 9120 Certified</h3> -->
			<p>
				Based on ISO 9001:2000, but with nearly 100 additional requirements specific aerospace, AS9120
				addresses chain of custody, traceability, control and availability of records.

			</p>
		</div><!--service features-->
		<div class="vertical-border-line-container">
			<div class="vertical-border-line"></div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="service-features wow animated fadeIn" data-wow-delay=".4s">
			<div class="img-container esd">
				<a target="_blank" href="/assets/documents/ANSIESD-S20-20-2007-2015-Cert.pdf"><img src="/assets/images/mit/ESD.png"></a>
			</div>
			<!-- <i class="ion-ios-color-wand-outline"></i> -->
			<!-- <h3>ESD Certified</h3> -->
			<p>
				ESD control program helps eliminate component failures before they occur, saving organizations time and money while increasing customer satisfaction.
			</p>
		</div><!--service features-->
	</div>
</div><!--row-->
</div>
</div>

<?php while (have_posts()) : the_post(); ?>
<?php the_content(); endwhile; ?>

<?php get_footer(); ?>