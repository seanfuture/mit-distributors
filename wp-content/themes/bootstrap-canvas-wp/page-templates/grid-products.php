<?php
/**
 * Template Name: Products Template
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

	<div class="header-img-container">
		<div class="header-top-line"></div>
		<div class="header-img"></div>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); endwhile; ?>

		<section id="work" class="section-padding wow animated fadeIn" data-wow-delay=".1s">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="text-center">
							<h2 class="center-title">Products</h2>
							<span class="center-border-line"></span>
							<p class="sub-text">
							From Connectors, through Batteries, Displays, Resistors, LCDs, Switches, Diodes, Transistors, Switches, Wire, Cable, and Tubing, let MIT Distributors be your source.
							</p>
						</div>
					</div>
				</div><!--center title row end-->
			</div><!--container-->

			<div class="container products-container one-line-captioned">

				<div id="filters-container" class="cbp-l-filters-dropdown">
					<div class="cbp-l-filters-dropdownWrap">
						<div class="cbp-l-filters-dropdownHeader">Sort Products</div>
						<div class="cbp-l-filters-dropdownList">
							<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
								All ( <div class="cbp-filter-counter"></div> categories )
							</div>
							<!-- <div data-filter=".print" class="cbp-filter-item">
								Print (<div class="cbp-filter-counter"></div> items)
							</div>
							<div data-filter=".web-design" class="cbp-filter-item">
								Web Design (<div class="cbp-filter-counter"></div> items)
							</div>
							<div data-filter=".motion" class="cbp-filter-item">
								Motion (<div class="cbp-filter-counter"></div> items)
							</div> -->
						</div>
					</div>
				</div>

				<div id="grid-container" class="cbp">

					<div class="cbp-item category">
						<a href="/assets/categories/001.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Batteries
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/002.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Capacitors
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/003.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Connectors
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/004.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Crystals, Oscillators &amp; Filters
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/005.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Diodes &amp; Transistors
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/006.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Displays
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/007.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Embedded Products
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/008.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Fans
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/009.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									LEDs
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/010.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Magnetics
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/011.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Power Supplies
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/012.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Relays
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/013.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Resistors
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/014.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Switches &amp; Sensors
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/015.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Wire, Cable &amp; Tools
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item category">
						<a href="/assets/categories/016.html" class="cbp-caption cbp-singlePageInline"
						   data-title="" onclick="attachRapidQuotePopup()">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Wireless, RF &amp; Microwave
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											View factory direct relationships
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>

				</div>

				<div id="loadMore-container" class="cbp-l-loadMore-button">
					<a href="#rapidquote" class="fancybox cbp-l-loadMore-link">
						<span class="cbp-l-loadMore-defaultText">RAPID QUOTE</span>
						<span class="cbp-l-loadMore-loadingText">LOADING...</span>
						<span class="cbp-l-loadMore-noMoreLoading">RAPID QUOTE</span>
					</a>
				</div>

			</div><!--container-->
		</section><!--portfolio section end here-->


	<?php get_footer(); ?>