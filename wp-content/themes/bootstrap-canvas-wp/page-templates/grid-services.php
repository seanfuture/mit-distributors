<?php
/**
 * Template Name: Services Template
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

	<div class="header-img-container">
		<div class="header-top-line"></div>
		<div class="header-img"></div>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); endwhile; ?>

		<section id="work" class="section-padding wow animated fadeIn" data-wow-delay=".1s">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="text-center">
							<h2 class="center-title">Services</h2>
							<span class="center-border-line"></span>
							<p class="sub-text">
								In addition to its traditional distribution model, MIT Distributors offers the following services:
							</p>
						</div>
					</div>
				</div><!--center title row end-->
			</div><!--container-->

			<div class="container services-container">


				<div id="filters-container" class="cbp-l-filters-dropdown">
					<div class="cbp-l-filters-dropdownWrap">
						<div class="cbp-l-filters-dropdownHeader">Sort Services</div>
						<div class="cbp-l-filters-dropdownList">
							<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
								All (<div class="cbp-filter-counter"></div> services)
							</div>
							<!-- <div data-filter=".print" class="cbp-filter-item">
								Print (<div class="cbp-filter-counter"></div> items)
							</div>
							<div data-filter=".web-design" class="cbp-filter-item">
								Web Design (<div class="cbp-filter-counter"></div> items)
							</div>
							<div data-filter=".motion" class="cbp-filter-item">
								Motion (<div class="cbp-filter-counter"></div> items)
							</div> -->
						</div>
					</div>
				</div>

				<div id="grid-container" class="cbp">

					<div class="cbp-item print motion">
						<a href="/assets/services/001.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Engineering and Design Support">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Engineering and Design Support
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/002.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Long Term Contracts">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Long Term Contracts
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/003.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Bonded Material Programs">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Bonded Material Programs
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/004.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Global Sourcing Capabilities">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Global Sourcing Capabilities
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/005.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Kitting Services">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Kitting Services
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/006.html" class="cbp-caption cbp-singlePageInline"
						   data-title="EOL and LTB Planning">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									EOL and LTB Planning
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/007.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Excess Inventory Solutions">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Excess Inventory Solutions
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/008.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Testing Services and Evaluation (In-House Testing Abilities)">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Testing Services and Evaluation (In-House Testing Abilities)
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

					<div class="cbp-item print motion">
						<a href="/assets/services/009.html" class="cbp-caption cbp-singlePageInline"
						   data-title="Parts Authentication Screening Services (PASS)">
							<div class="cbp-caption-defaultWrap">
								<p class="caption-content">
									Parts Authentication Screening Services (PASS)
								</p>
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="cbp-l-caption-alignLeft">
									<div class="cbp-l-caption-body">
										<div class="cbp-l-caption-title">
											Learn more
										</div>
										<!-- <div class="cbp-l-caption-desc">
											Electronic parts and component partner
										</div> -->
									</div>
								</div>
							</div>
						</a>
					</div>

				</div>

				<div id="loadMore-container" class="cbp-l-loadMore-button">
					<a href="#rapidquote" class="fancybox cbp-l-loadMore-link">
						<span class="cbp-l-loadMore-defaultText">RAPID QUOTE</span>
						<span class="cbp-l-loadMore-loadingText">LOADING...</span>
						<span class="cbp-l-loadMore-noMoreLoading">RAPID QUOTE</span>
					</a>
				</div>

			</div><!--container-->
		</section><!--portfolio section end here-->


	<?php get_footer(); ?>