<?php
/**
 * Template Name: Blank Page Template
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

	<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); endwhile; ?>

	<?php get_footer(); ?>