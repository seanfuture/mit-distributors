<?php
/**
 * Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title(); ?></title>

				<!-- Bootstrap -->
				<link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
				<!--custom css (Using style.scss file for customize)-->
				<link href="/assets/css/style.css" rel="stylesheet" type="text/css">
				<!--Ion icons-->
				<link href="/assets/icons/css/ionicons.min.css" rel="stylesheet" type="text/css">
				<!--google fonts-->
				<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,400italic,300italic,500,500italic,700,900' rel='stylesheet' type='text/css'>
				<!--Flex slider-->
				<link href="/assets/css/flexslider.css" rel="stylesheet">
				<!--popups css-->
				<link href="/assets/css/magnific-popup.css" rel="stylesheet">
				<!--animated css-->
				<link href="/assets/css/animate.css" rel="stylesheet">
				<!--cube portfolio-->
				<link href="/assets/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet">
				<!--pricing css-->
				<link href="/assets/css/pricing.css" rel="stylesheet" type="text/css">
				<!--revolution slider setting css-->
				<link href="/assets/rs-plugin/css/settings.css" rel="stylesheet">
				<!--master slider-->
				<link href="/assets/master-slider/style/masterslider.css" rel="stylesheet">
				<link href="/assets/master-slider/skins/default/style.css" rel="stylesheet">
				<link href="/assets/css/master-slider-custom.css" rel="stylesheet" type="text/css">
				<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
				<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
				<!--[if lt IE 9]>
					<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
					<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
				<![endif]-->

	<?php
		/*
		 * We add some JavaScript to pages with the comment form
		 * to support sites with threaded comments (when in use).
		 */
		if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/*
		<?php $header_image = get_header_image(); ?>

		<div class="blog-header" <?php if ( get_header_image() ) : ?>style="background-image: url( '<?php echo esc_url( $header_image ); ?>'); background-size: cover; background-repeat: no-repeat; background-position: top left; margin-bottom: 30px; width: 100%; height: 100%; min-height: <?php echo HEADER_IMAGE_HEIGHT; ?>px; position: relative;"<?php endif; ?>>
			<div class="container" <?php if ( get_header_image() ) : ?>style="height: auto; min-height: <?php echo HEADER_IMAGE_HEIGHT; ?>px; position: relative;"<?php endif; ?>>
				<?php if ( display_header_text() ) : ?>
				<?php $header_text_color = get_header_textcolor(); ?>
				<h1 class="blog-title" style="color: #<?php echo $header_text_color ?>;"><?php bloginfo( 'name' ); ?></h1>
				<p class="lead blog-description" style="color: #<?php echo $header_text_color ?>"><?php bloginfo( 'description' ); ?></p>
				<?php else : ?>
				<h1 class="blog-title" style="visibility: hidden; margin: 0; padding: 0; font-size: 0;"><?php bloginfo( 'name' ); ?></h1>
				<p class="lead blog-description" style="visibility: hidden; margin: 0; padding: 0; font-size: 0;"><?php bloginfo( 'description' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	 */

		/*
		 * Always have wp_head() just before the closing </head>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to add elements to <head> such
		 * as styles, scripts, and meta tags.
		 */
		wp_head();
		?>
	</head>
	<body <?php body_class(); ?> data-spy="scroll" data-target=".navbar" data-offset="80">

		<!-- Static navbar -->
		<nav class="navbar navbar-default sticky-nav navbar-static-top">
				<div class="container">
						<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
								</button>
								<div class="navbar-brand"><a href="/"><img src="/assets/images/mit/logo.png" alt="LOGO"></a></div>
								<div class="navbar-division">
									<label>a division of</label>
									<a href="http://www.worldmicro.com">
										<img src="/assets/images/mit/logo-world-micro.jpg" alt="World Micro">
									</a>
								</div>
						</div>
						<div id="navbar" class="navbar-collapse collapse">

								<?php
											wp_nav_menu( array(
												'menu'              => 'primary',
												'theme_location'    => 'primary',
												'depth'             => 2,
												'container'         => 'div',
												'container_class'   => 'collapse navbar-collapse',
												'container_id'      => 'bs-example-navbar-collapse-1',
												'menu_class'        => 'nav navbar-nav navbar-right scroll-to',
												'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
												'walker'            => new wp_bootstrap_navwalker())
											);
										?><!--/.nav-collapse -->

						</div><!--/.nav-collapse -->

				</div><!--/.container -->
		</nav>

		<div class="container header-cta">
			<div class="header-rapid-quote cbp-l-loadMore-button">
				<a href="#rapidquote" class="fancybox cbp-l-loadMore-link">
					<span class="cbp-l-loadMore-defaultText">RAPID QUOTE</span>
					<span class="cbp-l-loadMore-loadingText">LOADING...</span>
					<span class="cbp-l-loadMore-noMoreLoading">RAPID QUOTE</span>
				</a>
			</div>
		</div>


		<div style="display:none" class="fancybox-hidden">
		<div id="rapidquote" class="rapidquote-form">

			<div class="text-center">
			  <h2 class="center-title">Rapid Quote</h2>
			  <span class="center-border-line"></span>
			</div>

			<?php echo do_shortcode('[contact-form-7 id="42" title="Rapid Quote"]'); ?>

		</div>
		</div>

